module.exports = {
  pages: {
    'index': {
      entry: './src/pages/Home/main.js',
      template: 'public/index.html',
      title: 'Home',
      chunks: [ 'chunk-vendors', 'chunk-common', 'index' ]
    },
    'about': {
      entry: './src/pages/About/main.js',
      template: 'public/index.html',
      title: 'About',
      chunks: [ 'chunk-vendors', 'chunk-common', 'about' ]
    },
    'orgs': {
      entry: './src/pages/Orgs/main.js',
      template: 'public/index.html',
      title: 'Organizations',
      chunks: [ 'chunk-vendors', 'chunk-common', 'orgs' ]
    },
    'donate': {
      entry: './src/pages/Donate/main.js',
      template: 'public/index.html',
      title: 'Donate',
      chunks: [ 'chunk-vendors', 'chunk-common', 'donate' ]
    },
    'joinus': {
      entry: './src/pages/JoinUs/main.js',
      template: 'public/index.html',
      title: 'Join Us',
      chunks: [ 'chunk-vendors', 'chunk-common', 'joinus' ]
    }
  }
}
